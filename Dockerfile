# =============================================================================
# Python / Django Application Server
# =============================================================================
ARG PYTHON_VERSION=3.9
FROM registry.gitlab.com/thelabnyc/python-geoip:${PYTHON_VERSION} as web
ENV PYTHONUNBUFFERED 1

# Create directories
RUN mkdir -p /root \
             /usr/share/nltk_data \
             /data/public/media
WORKDIR /root

# Install apt packages:
#  1. OpenLDAP libraries and headers necessary for python-ldap
#  2. Hunspell libraries for spellchecking search queries
#  3. Pandoc for document format conversion
#  4. ImageMagick for Python Wand (Wagtail GIF support)
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -yq \
        build-essential \
        libhunspell-dev \
        libldap2-dev libsasl2-dev ldap-utils \
        libmagickwand-dev \
        pandoc \
    && \
    rm -rf /var/lib/apt/lists/* && \
    unset DEBIAN_FRONTEND

# Make volume for poetry's cache (to reduce image size)
VOLUME /root/.cache/pypoetry

# Download NLTK data
ENV NLTK_DATA /usr/share/nltk_data
RUN pip install nltk && \
    python -m nltk.downloader punkt -d $NLTK_DATA


# =============================================================================
# Python / Django Application Server WITH NodeJS / Puppeteer (for testing)
# =============================================================================
FROM web as puppeteer

# Install NodeJS and Yarn
ARG NODE_VERSION
ENV NODE_VERSION ${NODE_VERSION}
RUN curl -sL "https://deb.nodesource.com/setup_${NODE_VERSION}.x" | bash - && \
    mkdir -p /etc/apt/keyrings && \
    curl -sS "https://dl.yarnpkg.com/debian/pubkey.gpg" | gpg --dearmor -o "/etc/apt/keyrings/yarn.gpg" && \
    echo "deb [signed-by=/etc/apt/keyrings/yarn.gpg] https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -yq \
        build-essential \
        nodejs \
        yarn \
    && \
    rm -rf /var/lib/apt/lists/* && \
    unset DEBIAN_FRONTEND

# Install Chromium
# Major version must match what available here: https://www.npmjs.com/package/@sitespeed.io/chromedriver
# Also remember to update image tag name when changing this
ARG CHROME_VERSION
ENV CHROME_VERSION ${CHROME_VERSION}
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -yq \
        chromium="${CHROME_VERSION}.*" \
        chromium-common="${CHROME_VERSION}.*" \
    && \
    rm -rf /var/lib/apt/lists/* && \
    unset DEBIAN_FRONTEND

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV CHROME_PATH=/usr/bin/chromium
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium

# Install Varnish
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -yq \
        varnish \
    && \
    rm -rf /var/lib/apt/lists/* && \
    unset DEBIAN_FRONTEND
